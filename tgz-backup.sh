#!/bin/bash
# Create tar.gz file out of latest backups in /backup folder.

BASE_FLDR=/backup
WEB_BAK=$( ls -td $BASE_FLDR/web/*.0 )
DB_BAK=$( ls -trd $BASE_FLDR/db/* | tail -1)

MONTH=$(date +%B-%Y)

# Run only as root
if [ ! "$(id -u)" = "0" ]; then
        echo -e "Error:: $0 : Only root can run this script"
        exit 100
fi

# Create tgz base folder if doesn't exists
[ ! -d "${BASE_FLDR}/tgz" ] && mkdir -p ${BASE_FLDR}/tgz

# Remove older backups
rm -f ${BASE_FLDR}/tgz/*.tgz

# Create the tgz file if web/db/tgz folder exists.
if [ -d ${WEB_BAK} ] && [ -d ${DB_BAK} ] && [ -d ${BASE_FLDR}/tgz ]; then
	tar -zcf ${BASE_FLDR}/tgz/${MONTH}.tgz ${WEB_BAK} ${DB_BAK}
fi