#!/bin/bash
# Web Backup script.
# Created By:    Mohammed Salih
#                System Administrator
#                Date: Feb 4th 2014

# Source folder. Remember the '/' at the end, it is required for rsync to function as desired
SRC_FLDR=/www/
# Backup folder location
BASE_BAK_FLDR=/backup/web
# Full backup folder rotation threshold.
ROTATE="14"
# Backup folder prefix
PRFX="web-backup"
# From here, only edit if you know what you are doing.

if [ ! "$(id -u)" = "0" ]; then
        echo -e "Error:: $0 : Only root can run this script"
        exit 100
fi

# Create base backup folder if it doesn't exists
[ ! -d "${BASE_BAK_FLDR}/${PRFX}.0" ] && mkdir -p "${BASE_BAK_FLDR}/${PRFX}.0"

# Rotates older backup folders
if [ -d "${BASE_BAK_FLDR}/${PRFX}.0" ]; then
	# Remove oldest backup folder if exists
	[ -d "${BASE_BAK_FLDR}/${PRFX}.${ROTATE}" ] && rm -rf "${BASE_BAK_FLDR}/${PRFX}.${ROTATE}"
	# Rotate each folders one step up.
	for ((i=${ROTATE}-1;i>0;i--)) ; do
		[ -f $PRFX.$i ] && mv $PRFX.$i $PRFX.$(($i+1))
	done
	# Copy with hardlinks flag to speedup copy and save disk space, while retaining the changes on each day.
	cp -al "${BASE_BAK_FLDR}/${PRFX}.0" "${BASE_BAK_FLDR}/${PRFX}.1"
else
	echo "Base backup folder not found or not created. exiting"
	exit 101
fi

# Actuall backup happens here. Initial backup time maybe more , 
# but subsequent runs will be faster
if [ -d ${SRC_FLDR} ]; then
	rsync -a "${SRC_FLDR}" "${BASE_BAK_FLDR}/${PRFX}.0"
else
	exit 102
fi